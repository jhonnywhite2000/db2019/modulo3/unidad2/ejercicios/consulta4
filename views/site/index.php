<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de selección!</h1>
        <p class="lead">Modulo 3 Unidad 2</p>
    </div>

    <div class="body-content">
         <div class="row">
             <!--
             Boton de consulta
             -->
             
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 1</h3>
                    <p>Nombre y edad de los ciclistas que han ganado etapas</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta1a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            <!--
             Boton de consulta dos
             -->
             
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 2</h3>
                    <p>Nombre y edad de los ciclistas que han ganado puertos</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta2a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta2'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
             
             <!--
             Boton de consulta tres
             -->
             
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 3</h3>
                    <p>Nombre y edad de los ciclistas que han ganado etapas y puertos</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta3a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta3'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
         </div>   
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 4</h3>
                    <p>Listar el director de los equipos que tengan ciclistas que hayna ganado alguna etapa</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta4a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta4'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            
               <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 5</h3>
                    <p>Dorsal y nombre de los ciclistas que hayan llevado algún maillot</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta5a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta5'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
             <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 6</h3>
                    <p>Dorsal y nombre de los ciclistas que han llevado el maillot amarillo</p>
                    <?= Html::a('Active record', ['site/consulta6a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta6'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
         </div> 
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 7</h3>
                    <p>Dorsal de los ciclistas que hayan llevado algún maillot y que han ganado etapas</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta7a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta7'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 8</h3>
                    <p>Indicar el numero de las etapas que tengan puertos</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta8a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta8'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
             <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 9</h3>
                    <p>Indicar los kms de las etapas  que hayan ganado ciclistas del banesto y que tengan puertos</p>
                    <?= Html::a('Active record', ['site/consulta9a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta9'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
         </div> 
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 10</h3>
                    <p>Listar el numero de ciclistas que hayan ganado alguna etapa con puerto</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta10a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta10'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 11</h3>
                    <p>Indicar el nombre de los puertos que hayan sido ganados por ciclistas de banesto</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta11a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta11'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 12</h3>
                    <p>Lsitar el numero de etapas que tengan puerto que hayan sido ganados por ciclistas de banestos con más de 200kms</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta12a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta12'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
        </div> 
    </div>
</div>
