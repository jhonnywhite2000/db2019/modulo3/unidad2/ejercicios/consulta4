<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use app\models\Equipo;
use app\models\Maillot;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Etapa;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function  actionCrud(){
        return $this->render("gestion");
    }
    public function actionConsulta1() {
      // mediante DAO
         $numero=Yii::$app->
              db->
         createCommand("SELECT DISTINCT count(*) FROM puerto JOIN ciclista ON puerto.dorsal = ciclista.dorsal")
              ->queryScalar();
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT nombre, edad FROM etapa JOIN ciclista ON etapa.dorsal = ciclista.dorsal',
            
             ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'], 
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas",
             "titulo"=>"Consulta 1",
            "sql"=>"SELECT DISTINCT nombre, edad FROM etapa JOIN ciclista ON etapa.dorsal = ciclista.dorsal",
        ]);
        
        
    }
    public function  actionConsulta1a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
        'query' => Ciclista::find()
                ->distinct()
                ->select("nombre,edad")
                ->join('INNER JOIN', 'etapa', 'etapa.dorsal = ciclista.dorsal'),
                ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'], 
             "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas",
             "titulo"=>"Consulta 1",
            "sql"=>"SELECT DISTINCT nombre, edad FROM etapa JOIN ciclista ON etapa.dorsal = ciclista.dorsal",
        ]);
    }
    public function  actionConsulta2(){
      // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT DISTINCT DISTINCT count(*) FROM puerto JOIN ciclista ON puerto.dorsal = ciclista.dorsal")
              ->queryScalar();
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT nombre, edad FROM puerto JOIN ciclista ON puerto.dorsal = ciclista.dorsal',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'], 
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado puertos",
             "titulo"=>"Consulta 2",
            "sql"=>"SELECT DISTINCT nombre, edad FROM puerto JOIN ciclista ON puerto.dorsal = ciclista.dorsal",
        ]);
    }
    public function  actionConsulta2a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->distinct()
                ->select("nombre,edad")
                ->innerJoin('puerto', 'puerto.dorsal = ciclista.dorsal'),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'], 
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado puertos",
             "titulo"=>"Consulta 2",
            "sql"=>"SELECT DISTINCT nombre, edad FROM puerto JOIN ciclista ON puerto.dorsal = ciclista.dorsal",
        ]);
    }
    
    public function  actionConsulta3(){
      // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT  DISTINCT count(*) FROM (ciclista INNER JOIN etapa ON ciclista.dorsal = etapa.dorsal)
                INNER JOIN puerto ON ciclista.dorsal = puerto.dorsal")
              ->queryScalar();
              
        $dataProvider = new SqlDataProvider([
            'sql'=>' SELECT DISTINCT nombre, edad FROM (ciclista INNER JOIN etapa ON ciclista.dorsal = etapa.dorsal)
                INNER JOIN puerto ON ciclista.dorsal = puerto.dorsal',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'], 
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas y puertos",
             "titulo"=>"Consulta 3",
            "sql"=>" SELECT DISTINCT nombre, edad FROM (ciclista INNER JOIN etapa ON ciclista.dorsal = etapa.dorsal)
                INNER JOIN puerto ON ciclista.dorsal = puerto.dorsal",
        ]);
    }
    public function  actionConsulta3a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->distinct()
                ->select("nombre,edad")
                ->joinWith('etapas', true, 'INNER JOIN')
                ->joinWith('puertos', true, 'INNER JOIN'),
        ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'], 
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas y puertos",
             "titulo"=>"Consulta 3",
            "sql"=>" SELECT DISTINCT nombre, edad FROM (ciclista INNER JOIN etapa ON ciclista.dorsal = etapa.dorsal)
                INNER JOIN puerto ON ciclista.dorsal = puerto.dorsal",
        ]);
    }        
    public function  actionConsulta4(){
        // mediante DAO
        $numero=Yii::$app->
              db->
              createCommand("SELECT DISTINCT count(*) FROM equipo INNER JOIN ciclista ON equipo.nomequipo = ciclista.nomequipo 
            INNER JOIN etapa ON ciclista.dorsal = etapa.dorsal")
              ->queryScalar();
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT director FROM equipo INNER JOIN ciclista ON equipo.nomequipo = ciclista.nomequipo 
            INNER JOIN etapa ON ciclista.dorsal = etapa.dorsal',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['director'], 
            "enunciado"=>"Listar el director de los equipos que tengan ciclistas que hayna ganado alguna etapa",
             "titulo"=>"Consulta 4",
            "sql"=>"SELECT DISTINCT director FROM equipo INNER JOIN ciclista ON equipo.nomequipo = ciclista.nomequipo 
            INNER JOIN etapa ON ciclista.dorsal = etapa.dorsal",
        ]);
     
    }
    public function  actionConsulta4a(){
        // mediante active record
          $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                 ->select('equipo.*')
                ->distinct()
                ->innerJoin('etapa', 'etapa.dorsal = ciclista.dorsal')
                ->innerJoin('equipo', 'equipo.nomequipo = ciclista.nomequipo'),
              //'query'=> Equipo::find()->innerJoinWith('ciclistas',true)->innerJoin('etapa','etapa.dorsal=ciclista.dorsal')->distinct()->select("director")
                ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nomequipo0.director'], 
            "enunciado"=>"Listar el director de los equipos que tengan ciclistas que hayna ganado alguna etapa",
             "titulo"=>"Consulta 4",
            "sql"=>"SELECT DISTINCT director FROM equipo INNER JOIN ciclista ON equipo.nomequipo = ciclista.nomequipo 
            INNER JOIN etapa ON ciclista.dorsal = etapa.dorsal",
        ]);
    }    
    public function  actionConsulta5(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT DISTINCT count(*) FROM ciclista INNER JOIN lleva ON ciclista.dorsal = lleva.dorsal")
              ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT lleva.dorsal, ciclista.nombre FROM ciclista INNER JOIN lleva ON ciclista.dorsal = lleva.dorsal',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'nombre'], 
            "enunciado"=>"Dorsal y nombre de los ciclistas que hayan llevado algún maillot",
            "titulo"=>"Consulta 5",
            "sql"=>"SELECT DISTINCT lleva.dorsal, ciclista.nombre FROM ciclista INNER JOIN lleva ON ciclista.dorsal = lleva.dorsal",
        ]);
    }
    public function  actionConsulta5a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
               ->distinct() 
               ->select("lleva.dorsal dorsal, nombre")
               ->innerJoinwith('llevas', true),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'nombre'], 
            "enunciado"=>"Dorsal y nombre de los ciclistas que hayan llevado algún maillot",
            "titulo"=>"Consulta 5",
            "sql"=>"SELECT DISTINCT lleva.dorsal, ciclista.nombre FROM ciclista INNER JOIN lleva ON ciclista.dorsal = lleva.dorsal",
        ]);
    }    
    public function  actionConsulta6(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT count(*) total FROM ciclista GROUP BY nomequipo")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT ciclista.dorsal, ciclista.nombre FROM maillot INNER JOIN (ciclista INNER JOIN lleva ON ciclista.dorsal = lleva.dorsal)
  ON maillot.código = lleva.código WHERE maillot.color = "amarillo"',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'nombre'], 
            "enunciado"=>"Dorsal y nombre de los ciclistas que hayan llevado algún maillot",
             "titulo"=>"Consulta 6",
            "sql"=>"SELECT DISTINCT ciclista.dorsal, ciclista.nombre FROM maillot INNER JOIN (ciclista INNER JOIN lleva ON ciclista.dorsal = lleva.dorsal)
  ON maillot.código = lleva.código WHERE maillot.color = 'amarillo'",
        ]);
    }

    public function  actionConsulta6a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
               ->distinct() 
               ->select("ciclista.dorsal dorsal, nombre")
               ->innerJoinwith('llevas', true)
               ->innerJoin('maillot', 'maillot.código = lleva.código')
               ->where("color='amarillo'")
               ,
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'nombre'], 
             "enunciado"=>"Dorsal y nombre de los ciclistas que hayan llevado algún maillot",
             "titulo"=>"Consulta 6",
            "sql"=>"SELECT DISTINCT ciclista.dorsal, ciclista.nombre FROM maillot INNER JOIN (ciclista INNER JOIN lleva ON ciclista.dorsal = lleva.dorsal)
  ON maillot.código = lleva.código WHERE maillot.color = 'amarillo'",
        ]);
    }
    public function  actionConsulta7(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT DISTINCT count(*) FROM lleva INNER JOIN etapa ON lleva.numetapa = etapa.numetapa")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT lleva.dorsal FROM lleva INNER JOIN etapa ON lleva.numetapa = etapa.numetapa',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'], 
            "enunciado"=>"Dorsal de los ciclistas que hayan llevado algún maillot y que han ganado etapas",
             "titulo"=>"Consulta 7",
            "sql"=>"SELECT DISTINCT lleva.dorsal FROM lleva INNER JOIN etapa ON lleva.numetapa = etapa.numetapa",
        ]);
    }
    public function  actionConsulta7a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
               ->distinct() 
               ->select("lleva.dorsal dorsal")
               ->innerJoinwith('llevas', true)
               ,
            ]);
        return $this->render("resultado", [
             "resultados"=>$dataProvider,
            "campos"=>['dorsal'], 
            "enunciado"=>"Dorsal de los ciclistas que hayan llevado algún maillot y que han ganado etapas",
             "titulo"=>"Consulta 7",
            "sql"=>"SELECT DISTINCT lleva.dorsal FROM lleva INNER JOIN etapa ON lleva.numetapa = etapa.numetapa",
        ]);
    }
    public function  actionConsulta8(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT count(DISTINCT numetapa)FROM puerto")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT count(DISTINCT numetapa) total FROM puerto',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['total'], 
            "enunciado"=>"Indicar el numero de las etapas que tengan puertos",
             "titulo"=>"Consulta 8",
            "sql"=>"SELECT DISTINCT numetapa FROM puerto",
        ]);
    }
    public function  actionConsulta8a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()
               ->distinct() 
               ->select("count(distinct numetapa)total"),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['total'], 
            "enunciado"=>"Indicar el numero de las etapas que tengan puertos",
            "titulo"=>"Consulta 8",
            "sql"=>"SELECT DISTINCT numetapa FROM puerto",
        ]);
    }
    public function  actionConsulta9(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT count(distinct etapa.numetapa) FROM ciclista INNER JOIN 
                etapa ON ciclista.dorsal= etapa.dorsal INNER JOIN puerto
                ON etapa.numetapa = puerto.numetapa
                WHERE ciclista.nomequipo= 'banesto'")
                ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT etapa.kms, etapa.numetapa FROM ciclista INNER JOIN 
            etapa ON ciclista.dorsal= etapa.dorsal INNER JOIN puerto
            ON etapa.numetapa = puerto.numetapa
            WHERE ciclista.nomequipo= "banesto"',
            ]);
            return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['kms', 'numetapa'], 
            "enunciado"=>"Indicar los kms de las etapas  que hayan ganado ciclistas del banesto y que tengan puertos",
            "titulo"=>"Consulta 9",
            "sql"=>"SELECT DISTINCT etapa.kms, etapa.numetapa FROM ciclista INNER JOIN 
                etapa ON ciclista.dorsal= etapa.dorsal INNER JOIN puerto
                ON etapa.numetapa = puerto.numetapa
                WHERE ciclista.nomequipo= 'banesto'",
        ]);
    }
    
    public function  actionConsulta9a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
               ->distinct() 
               ->select("kms, etapa.numetapa numetapa")
               ->innerJoinwith('puertos', true)
               ->innerJoin('ciclista', 'ciclista.dorsal= etapa.dorsal')
               ->where("ciclista.nomequipo= 'banesto'")
               ,
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['kms', 'numetapa'], 
            "enunciado"=>"Indicar los kms de las etapas  que hayan ganado ciclistas del banesto y que tengan puertos",
            "titulo"=>"Consulta 9",
            "sql"=>"SELECT DISTINCT etapa.kms, etapa.numetapa FROM ciclista INNER JOIN 
                etapa ON ciclista.dorsal= etapa.dorsal INNER JOIN puerto
                ON etapa.numetapa = puerto.numetapa
                WHERE ciclista.nomequipo= 'banesto'",
        ]);
    }
    
    public function  actionConsulta10(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT COUNT(DISTINCT etapa.dorsal) FROM etapa INNER JOIN puerto ON etapa.numetapa = puerto.numetapa")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(DISTINCT etapa.dorsal) total FROM etapa INNER JOIN puerto ON etapa.numetapa = puerto.numetapa',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['total'], 
            "enunciado"=>"Listar el numero de ciclistas que hayan ganado alguna etapa con puerto",
            "titulo"=>"Consulta 10",
            "sql"=>"SELECT COUNT(DISTINCT etapa.dorsal) FROM etapa INNER JOIN puerto ON etapa.numetapa = puerto.numetapa",
        ]);
    }
    public function  actionConsulta10a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
               ->distinct() 
               ->select("count(distinct etapa.dorsal) total")
               ->innerJoinwith('puertos', true),
            ]);
        return $this->render("resultado", [
             "resultados"=>$dataProvider,
            "campos"=>['total'], 
            "enunciado"=>"Listar el numero de ciclistas que hayan ganado alguna etapa con puerto",
            "titulo"=>"Consulta 10",
            "sql"=>"SELECT COUNT(DISTINCT etapa.dorsal) FROM etapa INNER JOIN puerto ON etapa.numetapa = puerto.numetapa",
        ]);
    }
    public function  actionConsulta11(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT count(puerto.nompuerto) FROM ciclista  INNER JOIN puerto ON ciclista.dorsal = puerto.dorsal
            WHERE nomequipo ='banesto'")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT puerto.nompuerto FROM ciclista  INNER JOIN puerto ON ciclista.dorsal = puerto.dorsal
            WHERE nomequipo ="banesto"',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'], 
            "enunciado"=>"Indicar el nombre de los puertos que hayan sido ganados por ciclistas de banesto",
            "titulo"=>"Consulta 11",
            "sql"=>"SELECT puerto.nompuerto FROM ciclista  INNER JOIN puerto ON ciclista.dorsal = puerto.dorsal
            WHERE nomequipo ='banesto'",
        ]);
    }
    public function  actionConsulta11a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()
               ->distinct() 
               ->select("nompuerto")
               ->innerJoin('ciclista', 'ciclista.dorsal= puerto.dorsal')
               ->where("ciclista.nomequipo='banesto'"),
            ]);
        return $this->render("resultado", [
             "resultados"=>$dataProvider,
            "campos"=>['nompuerto'], 
            "enunciado"=>"Indicar el nombre de los puertos que hayan sido ganados por ciclistas de banesto",
            "titulo"=>"Consulta 11",
            "sql"=>"SELECT puerto.nompuerto FROM ciclista  INNER JOIN puerto ON ciclista.dorsal = puerto.dorsal
            WHERE nomequipo ='banesto'",
        ]);
    }
    public function  actionConsulta12(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT count(distinct etapa.numetapa) FROM ciclista INNER JOIN 
                etapa ON ciclista.dorsal= etapa.dorsal INNER JOIN puerto
                ON etapa.numetapa = puerto.numetapa
                WHERE ciclista.nomequipo= 'banesto' AND kms>=200")
                ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT etapa.kms, etapa.numetapa FROM ciclista INNER JOIN 
            etapa ON ciclista.dorsal= etapa.dorsal INNER JOIN puerto
            ON etapa.numetapa = puerto.numetapa
            WHERE ciclista.nomequipo= "banesto" AND kms>=200',
            ]);
            return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'], 
            "enunciado"=>"Indicar los kms de las etapas  que hayan ganado ciclistas del banesto y que tengan puertos con más de 200kms",
            "titulo"=>"Consulta 12",
            "sql"=>"SELECT DISTINCT etapa.kms, etapa.numetapa FROM ciclista INNER JOIN 
                etapa ON ciclista.dorsal= etapa.dorsal INNER JOIN puerto
                ON etapa.numetapa = puerto.numetapa
                WHERE ciclista.nomequipo= 'banesto' AND kms>=200",
        ]);
    }
    
    public function  actionConsulta12a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
               ->distinct() 
               ->select("etapa.numetapa numetapa")
               ->innerJoinwith('puertos', true)
               ->innerJoin('ciclista', 'ciclista.dorsal= etapa.dorsal')
               ->where("ciclista.nomequipo= 'banesto'")
               ->andWhere("kms >=200"),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'], 
            "enunciado"=>"Indicar los kms de las etapas  que hayan ganado ciclistas del banesto y que tengan puertos con más de 200kms",
            "titulo"=>"Consulta 12",
            "sql"=>"SELECT DISTINCT etapa.kms, etapa.numetapa FROM ciclista INNER JOIN 
                etapa ON ciclista.dorsal= etapa.dorsal INNER JOIN puerto
                ON etapa.numetapa = puerto.numetapa
                WHERE ciclista.nomequipo= 'banesto' AND kms>=200",
        ]);
    }
    }